mod cli_struct;
mod lib;
use anyhow::{anyhow, Result};
use clap::{Clap, IntoApp};
use clap_generate::{generate, generators};
use cli_struct::{Command, Shell};
use crossterm::style::{Colorize, Styler};
use lib::Powerplug;
use std::io::stdout;
use which::which;

impl Command {
    fn plug(&self) -> Result<Powerplug> {
        Ok(match self {
            Self::Install { force, ref uri, .. } => {
                Powerplug::clone(uri, *force)
                    .map_err(|_| anyhow!("Powercord is already installed"))?
            }
            _ => Powerplug::open().map_err(|_| anyhow!("Powercord is not installed"))?,
        })
    }

    fn run(&self, plug: &Powerplug) -> Result<()> {
        match self {
            Self::Plug | Self::Install { no_plug: false, .. } => plug.plug()?,
            Self::Unplug => plug.unplug()?,
            Self::Status => {
                println!(
                    "{plugged}, {rev} on {branch}",
                    plugged = if plug.plugged()? {
                        "Plugged".green()
                    } else {
                        "Not plugged".red()
                    }
                    .bold(),
                    rev = plug.revision()?.yellow(),
                    branch = plug.branch()?.blue()
                )
            }
            Self::Switch { ref branch } => plug.switch(branch)?,
            Self::Uninstall => {
                plug.unplug()?;
                plug.delete()?;
            }
            Self::Update => plug.update()?,
            Self::Install { no_plug: true, .. } => (),
            Self::Completions { shell } => {
                let mut app: clap::App = Self::into_app();
                let name = app
                    .get_bin_name()
                    .unwrap_or_else(|| app.get_name())
                    .to_owned();

                match shell {
                    Shell::Bash => generate::<generators::Bash, _>(&mut app, name, &mut stdout()),
                    Shell::Zsh => generate::<generators::Zsh, _>(&mut app, name, &mut stdout()),
                    Shell::Fish => generate::<generators::Fish, _>(&mut app, name, &mut stdout()),
                    Shell::Elvish => {
                        generate::<generators::Elvish, _>(&mut app, name, &mut stdout())
                    }
                    Shell::Powershell => {
                        generate::<generators::PowerShell, _>(&mut app, name, &mut stdout())
                    }
                }
            }
            Self::Path => println!("{}", Powerplug::location().display()),
        }
        Ok(())
    }
}
fn main() -> Result<()> {
    env_logger::init();
    for prog in ["npm", "git"].iter() {
        which(prog).map_err(|_| anyhow!("{} is not installed", prog))?;
    }
    let cli = Command::parse();
    cli.run(&cli.plug()?)?;
    Ok(())
}
