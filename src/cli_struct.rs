use crate::lib::OFFICIAL_REPO;
use anyhow::anyhow;
use clap::{
    AppSettings::{ColoredHelp, DontCollapseArgsInUsage, InferSubcommands, VersionlessSubcommands},
    Clap,
};
use std::{
    fmt::{self, Display, Formatter},
    str::FromStr,
};

#[derive(Debug, PartialEq, Eq, Clap)]
pub(crate) enum Shell {
    Bash,
    Zsh,
    Fish,
    Elvish,
    Powershell,
}
// https://github.com/clap-rs/clap/issues/459
impl FromStr for Shell {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "bash" => Ok(Shell::Bash),
            "zsh" => Ok(Shell::Zsh),
            "fish" => Ok(Shell::Fish),
            "elvish" => Ok(Shell::Elvish),
            "pwsh" | "powershell" => Ok(Shell::Powershell),
            _ => Err(anyhow!("Invalid shell")),
        }
    }
}

impl Display for Shell {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Shell::Bash => "bash",
            Shell::Zsh => "zsh",
            Shell::Fish => "fish",
            Shell::Elvish => "elvish",
            Shell::Powershell => "powershell",
        }
        .fmt(f)
    }
}

#[derive(Debug, Clap)]
#[clap(
    bin_name = "plug",
    about,
    author,
    global_setting(ColoredHelp),
    global_setting(DontCollapseArgsInUsage),
    global_setting(InferSubcommands),
    global_setting(VersionlessSubcommands)
)]
pub(crate) enum Command {
    /// Inject Powercord
    Plug,
    /// Tell Powercord revision, branch and whether it is injected
    Status,
    /// Completely uninstall Powercord
    Uninstall,
    /// Uninject Powercord
    Unplug,
    /// Update Powercord
    Update,
    /// Install Powercord
    Install {
        #[clap(default_value=OFFICIAL_REPO)]
        /// Powercord repository URI
        uri:     String,
        #[clap(short, long)]
        /// Install Powercord even if it is already installed
        force:   bool,
        /// Only download Powercord, do not inject
        #[clap(short, long)]
        no_plug: bool,
    },
    /// Change the Powercord branch
    Switch {
        /// Branch to switch to
        branch: String,
    },
    /// Generate completions for a shell
    Completions {
        /// Shell to generate completions for
        shell: Shell,
    },
    /// Get path to Powercord
    Path,
}
