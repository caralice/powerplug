mod inject;
mod plug;
pub use plug::Powerplug;

pub const OFFICIAL_REPO: &str = "https://github.com/powercord-org/powercord.git";
