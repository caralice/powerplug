use super::inject::{inject, injected, uninject};
use anyhow::{anyhow, Result};
use directories::ProjectDirs;
use git2::{build::CheckoutBuilder, Repository, ResetType};
use std::{fs, path::PathBuf, process::Command};

use log::info;
pub struct Powerplug {
    repo: git2::Repository,
}

impl Powerplug {
    #[must_use]
    pub fn location() -> PathBuf {
        ProjectDirs::from("dev", "powercord", "powercord")
            .unwrap()
            .data_dir()
            .to_owned()
    }

    pub fn open() -> Result<Self> {
        Ok(Self {
            repo: Repository::open(Self::location())?,
        })
    }

    pub fn clone(repo_url: &str, force: bool) -> Result<Self> {
        info!("Cloning the repo");
        if force && Self::location().exists() {
            fs::remove_dir_all(Self::location())?;
        }
        Ok(Self {
            repo: Repository::clone(repo_url, Self::location())?,
        })
    }

    pub fn delete(&self) -> std::io::Result<()> {
        fs::remove_dir_all(Self::location())
    }

    pub fn install_deps(&self) -> Result<()> {
        info!("Installing dependencies");
        Command::new("npm")
            .arg("install")
            .current_dir(Self::location())
            .spawn()?
            .wait()?;
        Ok(())
    }

    pub fn plug(&self) -> Result<()> {
        info!("Plugging Powercord");
        self.install_deps()?;
        inject(&Self::location())
    }

    pub fn unplug(&self) -> Result<()> {
        uninject(&Self::location())
    }

    pub fn switch(&self, branch: &str) -> Result<()> {
        let rev = self.repo.revparse_single(&format!("origin/{}", branch))?;
        self.repo
            .checkout_tree(&rev, Some(CheckoutBuilder::new().force()))?;
        self.repo.set_head(&format!("refs/heads/{}", branch))?;
        Ok(())
    }

    pub fn update(&self) -> Result<()> {
        let mut origin_remote = self.repo.find_remote("origin")?;
        let head = self.repo.head()?;
        let branch = head.shorthand().unwrap();
        origin_remote.fetch(&[branch], None, None)?;
        let oid = self
            .repo
            .refname_to_id(&format!("refs/remotes/origin/{}", branch))?;
        let object = self.repo.find_object(oid, None)?;
        self.repo.reset(&object, ResetType::Hard, None)?;
        Ok(())
    }

    pub fn branch(&self) -> Result<String> {
        Ok(self
            .repo
            .head()?
            .shorthand()
            .ok_or_else(|| anyhow!("Branch name is not a valid UTF-8"))?
            .to_owned())
    }

    pub fn revision(&self) -> Result<String> {
        Ok(self
            .repo
            .head()?
            .target()
            .ok_or_else(|| anyhow!("Couldn't get the target"))?
            .to_string())
    }

    pub fn plugged(&self) -> Result<bool> {
        injected(&Self::location())
    }
}
