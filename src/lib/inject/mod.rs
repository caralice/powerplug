#[cfg(target_os = "macos")]
#[path = "macos.rs"]
mod platform;
#[cfg(target_os = "windows")]
#[path = "windows.rs"]
mod platform;
#[cfg(target_os = "linux")]
#[path = "linux.rs"]
mod platform;
use anyhow::{anyhow, Result};
use log::{debug, info};
use platform::discord_dir;
use std::{
    fs::{create_dir_all, remove_dir_all, File},
    io::{BufWriter, Write},
    path::PathBuf,
};

fn app_dir() -> Result<PathBuf> {
    debug!("Getting the app directory");
    Ok(discord_dir()?.join("resources").join("app"))
}

pub fn inject(path: &PathBuf) -> Result<()> {
    debug!("Injecting Powercord");
    if injected(path)? {
        Err(anyhow!("Already injected"))
    } else {
        debug!("Powercord is not injected");
        let dir = app_dir()?;
        debug!("Creating the app directory");
        create_dir_all(&dir)?;
        debug!("Opening index.js");
        let mut file = BufWriter::new(File::create(dir.join("index.js"))?);
        debug!("Writing to index.js");
        writeln!(
            file,
            "require('{}')",
            path.join("src")
                .join("patcher.js")
                .canonicalize()?
                .to_str()
                .ok_or_else(|| anyhow!("patcher.js does not exist"))?
                .replace(&std::path::MAIN_SEPARATOR.to_string().repeat(2), "/")
                .replace(r"\", r"\\")
                .replace(r"'", r"\'")
        )?;
        debug!("Opening package.json");
        let mut file = BufWriter::new(File::create(dir.join("package.json"))?);
        debug!("Writing to package.json");
        file.write_all(r#"{"main": "index.js", "name": "discord"}"#.as_bytes())?;
        Ok(())
    }
}
pub fn uninject(path: &PathBuf) -> Result<()> {
    if !injected(path)? {
        Err(anyhow!("Not injected"))
    } else {
        Ok(remove_dir_all(app_dir()?)?)
    }
}
pub fn injected(_path: &PathBuf) -> Result<bool> {
    // TODO: check if **this** installation is injected
    info!("Checking if Powercord is already injected");
    Ok(app_dir()?.exists())
}
