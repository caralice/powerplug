use anyhow::{anyhow, Result};
use std::{env::var, fs::read_dir, path::PathBuf};

pub(crate) fn discord_dir() -> Result<PathBuf> {
    Ok(
        read_dir(PathBuf::from(var("LOCALAPPDATA")?).join("DiscordCanary"))?
            .filter_map(|i| i.ok())
            .filter(|i| {
                i.file_type().unwrap().is_dir()
                    && i.file_name().to_str().unwrap().starts_with("app-")
            })
            .last()
            .ok_or(anyhow!("no builds found"))?
            .path(),
    )
}
