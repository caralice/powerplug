use anyhow::{anyhow, Result};
use log::info;
use once_cell::sync::Lazy;
use regex::Regex;
use std::path::PathBuf;
use sysinfo::{ProcessExt, System, SystemExt};

static EXE_REGEX: Lazy<Regex> =
    Lazy::new(|| Regex::new("(?i)^discord-?canary$").expect("Couldn't compile regex"));

static KNOWN_PATHS: Lazy<Vec<PathBuf>> = Lazy::new(|| {
    vec![
        "/usr/share/discord-canary".into(),
        "/usr/lib64/discord-canary".into(),
        "/opt/discord-canary".into(),
        "/opt/DiscordCanary".into(),
        directories::BaseDirs::new()
            .unwrap()
            .executable_dir()
            .unwrap()
            .join("DiscordCanary"),
    ]
});

pub(crate) fn discord_dir() -> Result<PathBuf> {
    let mut info = System::new();
    info.refresh_processes();
    info!("Looking for Discord process…");
    info.get_processes()
        .iter()
        .map(|p| p.1)
        .find(|p| {
            p.cmd().join(" ").contains(&"--type=renderer".to_owned())
                && EXE_REGEX.is_match(p.exe().file_name().unwrap().to_str().unwrap())
        })
        .map(|p| p.exe().parent())
        .flatten()
        .map(|p| p.to_owned())
        .or_else(|| {
            info!("Couldn't find the process, falling back to known paths");
            KNOWN_PATHS
                .iter()
                .find(|p| p.exists() && p.is_dir())
                .map(|p| p.to_owned())
        })
        .ok_or_else(|| anyhow!("Not found"))
}
