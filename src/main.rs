#![feature(box_syntax)]
use clap::{app_from_crate, App, Arg};
use directories::ProjectDirs;
use exitfailure::ExitFailure;
use git2::{build::CheckoutBuilder, Repository, ResetType};
use std::fmt;
use std::fs;
use std::path::Path;
use std::process::{Command, Stdio};

const REPO: &str = "https://github.com/powercord-org/powercord";

#[derive(Debug)]
struct PlugError {
    msg: String,
}

impl PlugError {
    fn new(s: String) -> Self {
        Self { msg: s }
    }
}

impl fmt::Display for PlugError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.msg.fmt(f)
    }
}

impl std::error::Error for PlugError {}

fn maybe_sudo(args: &[&str]) -> Command {
    let mut c;
    if cfg!(target_os = "linux") {
        c = Command::new("sudo");
        c.args(args);
    } else {
        c = Command::new(args[0]);
        c.args(&args[1..]);
    }
    c
}

fn get_repo(location: &Path) -> Result<Repository, failure::Error> {
    if !location.exists() {
        Err(PlugError {
            msg: format!(
                "Can't find Powercord in {}; please install it first",
                location.display()
            ),
        }
        .into())
    } else {
        Ok(Repository::open(location)?)
    }
}

struct Status {
    plugged: bool,
    branch: Option<String>,
    revision: Option<String>,
}

impl Status {
    fn get(location: &Path) -> Result<Self, failure::Error> {
        let repo = Repository::open(location);
        Ok(match repo {
            Err(_) => Self {
                plugged: false,
                branch: None,
                revision: None,
            },
            Ok(r) => Self {
                revision: r.head()?.target().map(|i| i.to_string()),
                branch: r.head()?.shorthand().map(|s| s.to_owned()),
                plugged: {
                    let output = Command::new("node")
                        .arg("--eval")
                        .arg(
                            r#"
                            let { getAppDir } = require(`./${process.platform}.js`);
                            getAppDir().then(console.log);
                        "#,
                        )
                        .current_dir(location.join("injectors"))
                        .output()?
                        .stdout;
                    let path = std::str::from_utf8(&output)?.trim();
                    Path::new(path).exists()
                },
            },
        })
    }
}

fn status(location: &Path) -> Result<(), failure::Error> {
    let status = Status::get(location)?;
    println!(
        "\
Branch: {branch}
Revision: {revision}
Plugged: {plugged}",
        branch = status.branch.unwrap_or("Invalid".to_owned()),
        revision = status.revision.unwrap_or("Invalid".to_owned()),
        plugged = status.plugged.to_string()
    );
    Ok(())
}

fn install(location: &Path, force: bool, no_plug: bool) -> Result<(), failure::Error> {
    if location.exists() {
        println!("Checking if directory already exists");
        if force {
            println!("Deleting directory");
            fs::remove_dir_all(location)?;
        } else {
            return Err(PlugError::new(format!(
                "`{}` exists; use `--force` to force install",
                location.display()
            ))
            .into());
        }
    }
    println!("Cloning repo `{}` to `{}`", REPO, location.display());
    Repository::clone(REPO, location)?;
    println!("Installing dependencies");
    Command::new("npm")
        .arg("install")
        .current_dir(location)
        .stdout(Stdio::null())
        //.stderr(Stdio::null())
        .spawn()?
        .wait()?;
    if !no_plug {
        plug(location)?;
    }
    Ok(())
}

fn uninstall(location: &Path) -> Result<(), failure::Error> {
    get_repo(location)?;
    unplug(location)?;
    println!("Deleting `{}`", location.display());
    fs::remove_dir_all(location)?;
    Ok(())
}

fn unplug(location: &Path) -> Result<(), failure::Error> {
    get_repo(location)?;
    println!("Unplugging");
    maybe_sudo(&["npm", "run", "unplug"])
        .current_dir(location)
        .spawn()?
        .wait()?;
    Ok(())
}

fn plug(location: &Path) -> Result<(), failure::Error> {
    get_repo(location)?;
    println!("Plugging");
    maybe_sudo(&["npm", "run", "plug"])
        .current_dir(location)
        .spawn()?
        .wait()?;
    Ok(())
}

fn checkout(location: &Path, branch_name: &str) -> Result<(), failure::Error> {
    let repo = get_repo(location)?;
    let rev = repo.revparse_single(&("origin/".to_owned() + branch_name))?;
    repo.checkout_tree(&rev, Some(CheckoutBuilder::new().force()))?;
    repo.set_head(&("refs/heads/".to_owned() + branch_name))?;
    Ok(())
}

fn update(location: &Path) -> Result<(), failure::Error> {
    let repo = get_repo(location)?;
    let mut origin_remote = repo.find_remote("origin")?;
    let head = repo.head()?;
    let branch = head.shorthand().unwrap();
    origin_remote.fetch(&[branch], None, None)?;
    let oid = repo.refname_to_id(&("refs/remotes/origin/".to_owned() + branch))?;
    let object = repo.find_object(oid, None)?;
    repo.reset(&object, ResetType::Hard, None)?;
    Ok(())
}

fn main() -> Result<(), ExitFailure> {
    let dirs = ProjectDirs::from("org", "powercord", "powercord").unwrap();
    let repo_loc = dirs.data_dir();
    let app = app_from_crate!()
        .subcommand(App::new("plug").about("Injects Powercord"))
        .subcommand(App::new("unplug").about("Uninjects Powercord"))
        .subcommand(
            App::new("install")
                .about("Installs Powercord")
                .arg(
                    Arg::new("force")
                        .about("deletes directory if already exists")
                        .short('f')
                        .long("force"),
                )
                .arg(
                    Arg::new("no-plug")
                        .about("do not plug Powercord, only download")
                        .short('d')
                        .long("no-plug"),
                ),
        )
        .subcommand(App::new("uninstall").about("Uninstalls Powercord"))
        .subcommand(App::new("status").about("Reports injection status"))
        .subcommand(
            App::new("checkout")
                .about("Switches branch")
                .arg(Arg::new("branch").index(1).required(true)),
        )
        .subcommand(App::new("update").about("Updates Powercord"));
    let matches = app.get_matches();
    match matches.subcommand() {
        ("plug", _) => plug(repo_loc)?,
        ("unplug", _) => unplug(repo_loc)?,
        ("install", Some(args)) => install(
            repo_loc,
            args.is_present("force"),
            args.is_present("no-plug"),
        )?,
        ("uninstall", _) => uninstall(repo_loc)?,
        ("status", _) | ("", _) => status(repo_loc)?,
        ("checkout", Some(args)) => checkout(repo_loc, args.value_of("branch").unwrap())?,
        ("update", _) => update(repo_loc)?,
        _ => unimplemented!(),
    }
    Ok(())
}
